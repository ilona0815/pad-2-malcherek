
#include "TravelAgency.h"
#include "BookingInGeneral.h"
TravelAgency::TravelAgency() {
}

TravelAgency::~TravelAgency() {
}

int TravelAgency::createBooking(
        char type, string price, string fromDate, string toDate, string tripID, vector<string> details)
{
    if (findTrip(stoi(tripID)) == NULL){return -1;}
    long correctID = BookingInGeneral::getCurrentID();
    if(details.size()==4)correctID=stoi(details.at(3));
    string price_s = price;
    string tripID_s = tripID;
    int cID=findTrip(stoi(tripID))->getCustomerID();
    string customerID =to_string(cID);
    string customerName = findCustomer(cID)->getName();
    string one=details.at(0);
    string two=details.at(1);
    string three;
    if (details.size()==3)  three=details.at(2);
    switch (type) {
        case 'R':
        { 
            CarReservation* cr = new CarReservation(
                    correctID, price_s, fromDate, toDate, tripID_s, customerID, customerName, one,two,three);
            findTrip(stoi(tripID))->addBooking(cr); //Booking added to the trip
            allBookings.push_back(cr);
            break;
        }
        case 'H':
        {
            HotelBooking* hb = new HotelBooking(
                    correctID, price_s, fromDate, toDate, tripID_s, customerID, customerName, one, two);
            findTrip(stoi(tripID))->addBooking(hb); //Booking added to the trip
            allBookings.push_back(hb);
            break;
        }
        case 'F':
        {
            FlightBooking* fb = new FlightBooking(
                    correctID, price_s, fromDate, toDate, tripID_s, customerID, customerName,one,two,three);
            findTrip(stoi(tripID))->addBooking(fb); //Booking added to the trip
            allBookings.push_back(fb);
            break;
        }
    }
    return correctID;
}

BookingInGeneral* TravelAgency::findBooking(long id) {
    for (BookingInGeneral* bp : allBookings) {
        if (bp->getID() == id) cout << "yes";
        return bp;
    }
}

Trip* TravelAgency::findTrip(long id) {
    for (Trip* tp : allTrips) {
        if (tp->getID() == id) {
            return tp;
        }
    }
    return NULL;
}

Customer* TravelAgency::findCustomer(long id) {
    for (Customer* cp : allCustomers) {
        if (cp->getID() == id) {
            return cp;
        }
    }
    return NULL;
}

void TravelAgency::readfile() {
    ifstream myInputFile("bookings2.txt");
    string line;

    double priceAccumulator = 0;
    long carCounter=0;
    long hotelCounter=0;
    long flightCounter=0;

    while (getline(myInputFile, line)) {//get every line of the file
        istringstream streamOfLine(line);
        string part; //for the parts of the line
        vector<string> parsedBooking;
        while (getline(streamOfLine, part, '|')) {//fill vector of strings
            parsedBooking.push_back(part);
        }
        char type = parsedBooking.at(0).at(0);                         //char type
        //general parameters for every booking:
        long id=stoi(parsedBooking.at(2 - 1));                         //long id
        string price = parsedBooking.at(3 - 1);                        //string price
        string fromDate = parsedBooking.at(4 - 1);                     //string fromDate
        string toDate = parsedBooking.at(5 - 1);                       //string toDate
        string tripID = parsedBooking.at(6 - 1);                       //string tripID
        string customerID = parsedBooking.at(7 - 1);
        string customerName = parsedBooking.at(8 - 1);
        priceAccumulator += stod(price);
        //customers and trips creation:
        if (findTrip(stoi(tripID)) == NULL)
        {
            Trip* tp = new Trip(tripID, customerID);
            allTrips.push_back(tp);
        }
        if (findCustomer(stoi(customerID)) == NULL)
        {
            Customer* cp = new Customer(customerID, customerName);
            allCustomers.push_back(cp);
        }
        findCustomer(stoi(customerID))->addTrip(findTrip(stoi(tripID))); //assign trip to customer
        vector<string>details;
        details.push_back(parsedBooking.at(9 - 1));
        details.push_back(parsedBooking.at(10 - 1));
        if(parsedBooking.size()==11) {details.push_back(parsedBooking.at(9 - 1));}else{details.push_back("");}
        details.push_back(parsedBooking.at(2 - 1));
        createBooking(type, price, fromDate, toDate, tripID, details);
        
        switch (type) {
            case 'R':
            {
                carCounter++;
                break;
            }
            case 'H':
            {
                hotelCounter++;
                break;
            }
            case 'F':
            {
                flightCounter++;
                break;
            }
            default:
            {
                cerr << "unknown";
            }
        }
    }

    cout << "Total value: " << priceAccumulator << " EUR" << endl<<
            "Total number of allCustomers: " << allCustomers.size() << endl<<
            "Total number of allBookings: "<< allBookings.size()<<endl<<
            "Total number of allTrips: "<<allTrips.size()<<endl<<
            "Car Bookings: "<<carCounter<<endl<<
            "Hotel Bookings: "<<hotelCounter<<endl<<
            "Flight Bookings: "<<flightCounter<<endl<<flush;
    findCustomer(1)->printTrips();
    findTrip(17)->printBookings();
}