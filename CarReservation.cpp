

#include "CarReservation.h"

CarReservation::CarReservation(long id,string price,
                                string fromDate,string toDate,
                                string tripID,string customerID,string customerName,
                                string pickupLocation,string returnLocation,string company)


:BookingInGeneral(id,price,fromDate,toDate,tripID,customerID,customerName)//call BookingIG constructor for id
{
    //do the rest of constructor for car reservation
    this->pickupLocation=pickupLocation;
    this->returnLocation=returnLocation;
    this->company=company;
}

CarReservation::~CarReservation() {
}

void CarReservation::print(){
    cout<<"CarReservation "<<id<<endl;
}