

#include "Customer.h"

Customer::Customer(string customerID,string customerName) {
    this->id=stoi(customerID);
    this->name=customerName;
}
void Customer::addTrip(Trip* theTrip){
    myTrips.push_back(theTrip);
}
long Customer::getID(){
    return id;
}
string Customer::getName(){
    return name;
}
void Customer::printTrips(){
    cout<<"Customer with id "<<id<<" has "<<myTrips.size()<<" trips"<<endl<<flush;
}
Customer::~Customer() {
}

