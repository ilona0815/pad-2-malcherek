
#include "HotelBooking.h"

HotelBooking::HotelBooking(long id,string price,
                            string fromDate,string toDate,
                            string tripID,string customerID,string customerName,
                            string hotel,string town)

:BookingInGeneral(id,price,fromDate,toDate,tripID,customerID,customerName)//call BookingIG constructor for id
{
    this->hotel=hotel;
    this->town=town;
}

HotelBooking::~HotelBooking() {
}

void HotelBooking::print(){
    cout<<"HotelBooking "<<id<<endl;
}