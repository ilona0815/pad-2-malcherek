

#ifndef TRIP_H
#define TRIP_H
#include "BookingInGeneral.h"
#include "general.h"
//#include "Customer.h"

class Trip {
public:
    Trip(string,string);
    void addBooking(BookingInGeneral* theBooking);
    long getID();
    long getCustomerID();
    void printBookings();
    virtual ~Trip();
private:
    long id;
    long customerID;
    vector<BookingInGeneral*> relatedBookings;
};

#endif /* TRIP_H */

